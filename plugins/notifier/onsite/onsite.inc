<?php

$plugin = array(
  'title' => t('On site'),
  'description' => t('Record association between message and audience.'),
  'class' => 'MessageNotifyOnsite',
);
