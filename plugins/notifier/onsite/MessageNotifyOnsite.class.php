<?php

/**
 * Email notifier.
 */
class MessageNotifyOnsite extends MessageNotifierBase {

  /*
   * This plugin has no view module, we are only establishing relationships.
   */
  public static function viewModes() {
    return array(
      'message_notify_onsite_mid' => array('label' => t('Notify - Message id')),
    );
  }
  /**
   * Our delivery is simply recording the message for the subscriber.
   */
  public function deliver(array $output = array()) {
    $plugin = $this->plugin;
    $message = $this->message;

    return drupal_write_record('message_notify_audience', $message);
  }
}
